<?php

/**
 * @file
 * Layout Builder Component Attributes module.
 */

/**
 * Implements template_preprocess_block().
 */
function layout_builder_block_language_preprocess_block(&$variables) {

}

/**
 * Implements hook_contextual_links_alter().
 */
function layout_builder_block_language_contextual_links_alter(array &$links, $group, array $route_parameters) {
  if ($group == 'layout_builder_block' && isset($links['layout_builder_block_language'])) {
    // Link weights are not respected, so a glorified array splice is used
    // instead. The 'Manage attributes' link should be inserted immediately
    // after the 'Configure' link.
    $insert_array['layout_builder_block_language'] = $links['layout_builder_block_language'];
    unset($links['layout_builder_block_language']);
    $array_keys = array_flip(array_keys($links));
    $update_pos = $array_keys['layout_builder_block_update'];

    $links = layout_builder_block_language_array_splice_assoc($links, $insert_array, $update_pos + 1);
  }
}

/**
 * Insert an associative array into a specific position in an array.
 *
 * See https://jboullion.com/array-splice-associative/.
 *
 * @param array $original
 *   The original array.
 * @param array $new
 *   The new array of values to insert into the original array.
 * @param int $offset
 *   The position in the array ( 0 index ) where the new array should
 *   be inserted.
 *
 * @return array
 *   The new combined array
 */
function layout_builder_block_language_array_splice_assoc(array $original, array $new, int $offset) {
  return array_slice($original, 0, $offset, TRUE) + $new + array_slice($original, $offset, NULL, TRUE);
}
