<?php

namespace Drupal\layout_builder_block_language\Form;

use Drupal\Core\Ajax\AjaxFormHelperTrait;
use Drupal\Core\Condition\ConditionManager;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\layout_builder\Controller\LayoutRebuildTrait;
use Drupal\layout_builder\LayoutBuilderHighlightTrait;
use Drupal\layout_builder\LayoutTempstoreRepositoryInterface;
use Drupal\layout_builder\SectionStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Plugin\ContextAwarePluginInterface;

/**
 * Provides a form for managing block language.
 */
class ManageComponentLanguageForm extends FormBase {

  use AjaxFormHelperTrait;
  use LayoutBuilderHighlightTrait;
  use LayoutRebuildTrait;

  /**
   * The section storage.
   *
   * @var \Drupal\layout_builder\SectionStorageInterface
   */
  protected $sectionStorage;

  /**
   * The section delta.
   *
   * @var int
   */
  protected $delta;

  /**
   * The component uuid.
   *
   * @var string
   */
  protected $uuid;

  /**
   * The Layout Tempstore.
   *
   * @var \Drupal\layout_builder\LayoutTempstoreRepositoryInterface
   */
  protected $layoutTempstore;

  /**
   * The condition manager.
   *
   * @var \Drupal\Core\Condition\ConditionManager
   */
  protected $conditionManager;

  /**
   * The condition plugin being configured.
   *
   * @var \Drupal\Core\Condition\ConditionInterface
   */
  protected $condition;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * Constructs a new ManageComponentPermissionForm.
   *
   * @param \Drupal\layout_builder\LayoutTempstoreRepositoryInterface $layout_tempstore_repository
   *   The layout tempstore.
   * @param \Drupal\Core\Condition\ConditionManager $condition_manager
   *   The condition plugin manager.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   */
  public function __construct(LayoutTempstoreRepositoryInterface $layout_tempstore_repository, ConditionManager $condition_manager, LanguageManagerInterface $language_manager) {
    $this->layoutTempstore = $layout_tempstore_repository;
    $this->conditionManager = $condition_manager;
    $this->languageManager = $language_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('layout_builder.tempstore_repository'),
      $container->get('plugin.manager.condition'),
      $container->get('language_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'layout_builder_manage_language_form';
  }

  /**
   * Builds the attributes form.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param \Drupal\layout_builder\SectionStorageInterface $section_storage
   *   The section storage being configured.
   * @param int $delta
   *   The original delta of the section.
   * @param string $uuid
   *   The UUID of the block being updated.
   *
   * @return array
   *   The form array.
   */
  public function buildForm(array $form, FormStateInterface $form_state, SectionStorageInterface $section_storage = NULL, $delta = NULL, $uuid = NULL) {
    $parameters = array_slice(func_get_args(), 2);
    foreach ($parameters as $parameter) {
      if (is_null($parameter)) {
        throw new \InvalidArgumentException('ManageComponentLanguageForm requires all parameters.');
      }
    }

    $this->sectionStorage = $section_storage;
    $this->delta = $delta;
    $this->uuid = $uuid;

    $section = $section_storage->getSection($delta);
    $component = $section->getComponent($uuid);
    $component_attributes = $component->get('component_language_attributes');
    $plugin_id = "language";
    $this->condition = $this->prepareCondition($plugin_id, $uuid, []);
    $form['#attributes']['data-layout-builder-target-highlight-id'] = $this->blockUpdateHighlightId($uuid);

    $form['#tree'] = TRUE;
    if ($this->languageManager->isMultilingual()) {
      // Fetch languages.
      $languages = $this->languageManager->getLanguages();
      $langcodes_options = [];
      foreach ($languages as $language) {
        $langcodes_options[$language->getId()] = $language->getName();
      }
      $form['langcodes'] = [
        '#type' => 'checkboxes',
        '#title' => $this->t('Language selection'),
        '#default_value' => $component_attributes[$uuid]["langcodes"],
        '#options' => $langcodes_options,
        '#description' => $this->t('Select languages to enforce. If none are selected, all languages will be allowed.'),
      ];
    }
    else {
      $form['langcodes'] = [
        '#type' => 'value',
        '#default_value' => $component_attributes[$uuid]["langcodes"],
      ];
    }
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Update'),
      '#button_type' => 'primary',
    ];

    if ($this->isAjax()) {
      $form['actions']['submit']['#ajax']['callback'] = '::ajaxSubmit';
    }
    return $form;
  }

  /**
   * Prepares the condition plugin based on the condition ID.
   *
   * @param string $condition_id
   *   A condition UUID, or the plugin ID used to create a new condition.
   * @param string $uuid
   *   A condition UUID.
   * @param array $value
   *   The condition configuration.
   *
   * @return \Drupal\Core\Condition\ConditionInterface
   *   The condition plugin.
   */
  protected function prepareCondition($condition_id, $uuid, array $value) {
    if ($value) {
      return $this->conditionManager->createInstance($value['id'], $value);
    }

    /** @var \Drupal\Core\Condition\ConditionInterface $condition */
    $condition = $this->conditionManager->createInstance($condition_id);
    $configuration = $condition->getConfiguration();
    $configuration['uuid'] = $uuid;
    $condition->setConfiguration($configuration);
    return $condition;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $delta = $this->getSelectedDelta($form_state);
    $section = $this->sectionStorage->getSection($delta);

    // If this block is context-aware, set the context mapping.
    if ($this->condition instanceof ContextAwarePluginInterface) {
      $this->condition->setContextMapping(["language" => "@language.current_language_context:language_interface"]);
    }
    $values = $form_state->getValues();
    $configuration = $this->condition->getConfiguration();
    $configuration['langcodes'] = $values['langcodes'];
    $visibility_conditions[$configuration['uuid']] = $configuration;
    // Store configuration in layout_builder.component.additional.
    // Switch to third-party settings when
    // https://www.drupal.org/project/drupal/issues/3015152 is committed.
    $section->getComponent($this->uuid)->set('component_language_attributes', $visibility_conditions);

    $this->layoutTempstore->set($this->sectionStorage);
    $form_state->setRedirectUrl($this->sectionStorage->getLayoutBuilderUrl());
  }

  /**
   * {@inheritdoc}
   */
  protected function successfulAjaxSubmit(array $form, FormStateInterface $form_state) {
    return $this->rebuildAndClose($this->sectionStorage);
  }

  /**
   * Gets the selected delta.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return int
   *   The section delta.
   */
  protected function getSelectedDelta(FormStateInterface $form_state) {
    if ($form_state->hasValue('region')) {
      return (int) explode(':', $form_state->getValue('region'))[0];
    }
    return (int) $this->delta;
  }

}
